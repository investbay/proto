{
  inputs = {
    nix.url = "git+ssh://git@git.investbay.dev/morosystems/investbay/devops/nix?branch=main";
  };

  outputs = { self, nix }: {
    formatter = nix.formatter;

    devShells = nix.lib.forAllSystems (pkgs:
      let
        go = pkgs.go_1_21;

        buildGoModule = pkgs.buildGoModule.override { inherit go; };
        golangci-lint = pkgs.golangci-lint.override { inherit buildGoModule; };

        protoc-gen-connect-go = pkgs.buildGoModule rec {
          version = "v1.16.2";
          pname = "protoc-gen-connect-go";
          src = pkgs.fetchFromGitHub {
            owner = "connectrpc";
            repo = "connect-go";
            rev = version;
            sha256 = "sha256-Ej8ya2sKtRVRQdMr63YpPbqzwtV0ZsqO+7xiif3gFr0=";
          };
          subPackages = [ "./cmd/protoc-gen-connect-go" ];
          vendorHash = "sha256-OKTRxExgJ2V3736fIVvQX3FEa44jGngwCi2D/uk0F58=";
        };

        buf-gen-config = {
          version = "v1";
          managed = {
            enabled = true;
          };
          plugins = [
            {
              name = "go";
              out = ".";
              opt = "module=go.investbay.dev/proto";
            }
            {
              name = "go-connect";
              out = ".";
              opt = "module=go.investbay.dev/proto";
              path = "${protoc-gen-connect-go}/bin/protoc-gen-connect-go";
            }
          ];
        };

        buf-gen-config-client = {
          version = "v1";
          managed = {
            enabled = true;
          };
          plugins = [
            {
              plugin = "buf.build/bufbuild/es";
              out = "pkg-ts/gen";
              opt = "target=js+dts";
            }
            {
              plugin = "buf.build/bufbuild/connect-es";
              out = "pkg-ts/gen";
              opt = "target=js+dts";
            }
          ];
        };
      in
      {
        default = pkgs.devshell.mkShell {
          name = "investbay-proto";

          packages = [
            pkgs.buf
            pkgs.protobuf
            pkgs.protoc-gen-go

            go
          ];

          commands = [
            {
              name = "lint";
              help = "check formatting";
              command = ''
                ${nix.lib.cd_root}
                ${golangci-lint}/bin/golangci-lint run --sort-results --out-format tab --config ${nix.lib.golangci-config-file} ./...
                buf lint ./proto
              '';
            }
            {
              name = "fix";
              help = "format";
              command = ''
                ${nix.lib.cd_root}
                ${pkgs.nixpkgs-fmt}/bin/nixpkgs-fmt .
                ${golangci-lint}/bin/golangci-lint run --sort-results --out-format tab --fix --issues-exit-code 0 --config ${nix.lib.golangci-config-file} ./...
                buf format -w ./proto
              '';
            }
            {
              name = "generate-3rd-party-proto";
              command = ''
                ${nix.lib.cd_root}
                protoPath="./.3rd-party-proto"
                errdetailsCommit="9516e70a70d6f7e0cd000ddd370d73656da09b96"

                [[ -f "''${protoPath}/google/rpc/error_details.version" ]] && [[ ''$(cat "''${protoPath}/google/rpc/error_details.version" | head -n 1) == "''${errdetailsCommit}" ]] && exit 0

                mkdir -p "''${protoPath}/google/rpc"
                curl -o ''${protoPath}/google/rpc/error_details.proto https://raw.githubusercontent.com/googleapis/googleapis/''${errdetailsCommit}/google/rpc/error_details.proto -s && \
                echo -n ''${errdetailsCommit} | tee ''${protoPath}/google/rpc/error_details.version > /dev/null
              '';
            }
            {
              name = "proto";
              help = "regenerate proto files";
              command = ''
                ${nix.lib.cd_root}
                buf generate --template '${builtins.toJSON buf-gen-config}' --output . ./proto
                buf generate --template '${builtins.toJSON buf-gen-config-client}' --path ./proto/investbay --output . ./proto

                generate-3rd-party-proto
                buf generate --template '${builtins.toJSON buf-gen-config-client}' --output . ./.3rd-party-proto
              '';
            }
            {
              name = "clean";
              help = "remove generated files";
              command = ''
                ${nix.lib.cd_root}
                find ./pkg-ts/gen -type f \( -name '*_pb.d.ts' -o -name '*_pb.js' -o -name '*ClientPb.ts' -o -name '*_connect.js' -o -name '*_connect.d.ts' \) -delete
                find ./pkg-ts/gen -type d -empty -delete

                find ./pkg-go/gen -type f -name '*.pb.go' -delete
                find ./pkg-go/gen -type f -name '*.connect.go' -delete
                find ./pkg-go/gen -type d -empty -delete
              '';
            }
            {
              name = "publish";
              help = ''
                Publish the packages, giving a git tag as first argument
                - npm, requires credentials to @investbay scope
              '';
              command = ''
                # [[ $(git status --porcelain) != "" ]] && echo "Please commit or discard your latest changes" && exit 1
                [[ $# -eq 0 ]] && echo "Please provide a tag as the first parameter" && exit 1
                TAG=$1

                [[ ''${TAG:0:1} == "v" ]] && echo "Provide the tag without the 'v' prefix" && exit 1

                ${nix.lib.cd_root}
                cd pkg-ts

                ${pkgs.nodejs_20}/bin/npm version $TAG --allow-same-version=true --git-tag-version=false
                git add package.json
                git commit -m "Update proto to version v''${TAG}"
                git tag -a v''${TAG} -m ""
                ${pkgs.nodejs_20}/bin/npm publish --access public
                echo "Please push the new git commits and tag to the remote"
              '';
            }
          ];
        };
      }
    );
  };
}
