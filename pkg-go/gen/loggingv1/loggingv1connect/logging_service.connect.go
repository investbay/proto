// Code generated by protoc-gen-connect-go. DO NOT EDIT.
//
// Source: investbay/logging/v1/logging_service.proto

package loggingv1connect

import (
	connect "connectrpc.com/connect"
	context "context"
	errors "errors"
	loggingv1 "go.investbay.dev/proto/pkg-go/gen/loggingv1"
	http "net/http"
	strings "strings"
)

// This is a compile-time assertion to ensure that this generated file and the connect package are
// compatible. If you get a compiler error that this constant is not defined, this code was
// generated with a version of connect newer than the one compiled into your binary. You can fix the
// problem by either regenerating this code with an older version of connect or updating the connect
// version compiled into your binary.
const _ = connect.IsAtLeastVersion1_13_0

const (
	// LoggingServiceName is the fully-qualified name of the LoggingService service.
	LoggingServiceName = "investbay.logging.v1.LoggingService"
)

// These constants are the fully-qualified names of the RPCs defined in this package. They're
// exposed at runtime as Spec.Procedure and as the final two segments of the HTTP route.
//
// Note that these are different from the fully-qualified method names used by
// google.golang.org/protobuf/reflect/protoreflect. To convert from these constants to
// reflection-formatted method names, remove the leading slash and convert the remaining slash to a
// period.
const (
	// LoggingServiceInsertLogEntryProcedure is the fully-qualified name of the LoggingService's
	// InsertLogEntry RPC.
	LoggingServiceInsertLogEntryProcedure = "/investbay.logging.v1.LoggingService/InsertLogEntry"
)

// These variables are the protoreflect.Descriptor objects for the RPCs defined in this package.
var (
	loggingServiceServiceDescriptor              = loggingv1.File_investbay_logging_v1_logging_service_proto.Services().ByName("LoggingService")
	loggingServiceInsertLogEntryMethodDescriptor = loggingServiceServiceDescriptor.Methods().ByName("InsertLogEntry")
)

// LoggingServiceClient is a client for the investbay.logging.v1.LoggingService service.
type LoggingServiceClient interface {
	InsertLogEntry(context.Context, *connect.Request[loggingv1.InsertLogEntryRequest]) (*connect.Response[loggingv1.InsertLogEntryResponse], error)
}

// NewLoggingServiceClient constructs a client for the investbay.logging.v1.LoggingService service.
// By default, it uses the Connect protocol with the binary Protobuf Codec, asks for gzipped
// responses, and sends uncompressed requests. To use the gRPC or gRPC-Web protocols, supply the
// connect.WithGRPC() or connect.WithGRPCWeb() options.
//
// The URL supplied here should be the base URL for the Connect or gRPC server (for example,
// http://api.acme.com or https://acme.com/grpc).
func NewLoggingServiceClient(httpClient connect.HTTPClient, baseURL string, opts ...connect.ClientOption) LoggingServiceClient {
	baseURL = strings.TrimRight(baseURL, "/")
	return &loggingServiceClient{
		insertLogEntry: connect.NewClient[loggingv1.InsertLogEntryRequest, loggingv1.InsertLogEntryResponse](
			httpClient,
			baseURL+LoggingServiceInsertLogEntryProcedure,
			connect.WithSchema(loggingServiceInsertLogEntryMethodDescriptor),
			connect.WithClientOptions(opts...),
		),
	}
}

// loggingServiceClient implements LoggingServiceClient.
type loggingServiceClient struct {
	insertLogEntry *connect.Client[loggingv1.InsertLogEntryRequest, loggingv1.InsertLogEntryResponse]
}

// InsertLogEntry calls investbay.logging.v1.LoggingService.InsertLogEntry.
func (c *loggingServiceClient) InsertLogEntry(ctx context.Context, req *connect.Request[loggingv1.InsertLogEntryRequest]) (*connect.Response[loggingv1.InsertLogEntryResponse], error) {
	return c.insertLogEntry.CallUnary(ctx, req)
}

// LoggingServiceHandler is an implementation of the investbay.logging.v1.LoggingService service.
type LoggingServiceHandler interface {
	InsertLogEntry(context.Context, *connect.Request[loggingv1.InsertLogEntryRequest]) (*connect.Response[loggingv1.InsertLogEntryResponse], error)
}

// NewLoggingServiceHandler builds an HTTP handler from the service implementation. It returns the
// path on which to mount the handler and the handler itself.
//
// By default, handlers support the Connect, gRPC, and gRPC-Web protocols with the binary Protobuf
// and JSON codecs. They also support gzip compression.
func NewLoggingServiceHandler(svc LoggingServiceHandler, opts ...connect.HandlerOption) (string, http.Handler) {
	loggingServiceInsertLogEntryHandler := connect.NewUnaryHandler(
		LoggingServiceInsertLogEntryProcedure,
		svc.InsertLogEntry,
		connect.WithSchema(loggingServiceInsertLogEntryMethodDescriptor),
		connect.WithHandlerOptions(opts...),
	)
	return "/investbay.logging.v1.LoggingService/", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		switch r.URL.Path {
		case LoggingServiceInsertLogEntryProcedure:
			loggingServiceInsertLogEntryHandler.ServeHTTP(w, r)
		default:
			http.NotFound(w, r)
		}
	})
}

// UnimplementedLoggingServiceHandler returns CodeUnimplemented from all methods.
type UnimplementedLoggingServiceHandler struct{}

func (UnimplementedLoggingServiceHandler) InsertLogEntry(context.Context, *connect.Request[loggingv1.InsertLogEntryRequest]) (*connect.Response[loggingv1.InsertLogEntryResponse], error) {
	return nil, connect.NewError(connect.CodeUnimplemented, errors.New("investbay.logging.v1.LoggingService.InsertLogEntry is not implemented"))
}
