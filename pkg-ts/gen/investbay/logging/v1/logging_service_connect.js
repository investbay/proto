// @generated by protoc-gen-connect-es v0.13.0 with parameter "target=js+dts"
// @generated from file investbay/logging/v1/logging_service.proto (package investbay.logging.v1, syntax proto3)
/* eslint-disable */
// @ts-nocheck

import { InsertLogEntryRequest, InsertLogEntryResponse } from "./logging_service_pb.js";
import { MethodKind } from "@bufbuild/protobuf";

/**
 * @generated from service investbay.logging.v1.LoggingService
 */
export const LoggingService = {
  typeName: "investbay.logging.v1.LoggingService",
  methods: {
    /**
     * @generated from rpc investbay.logging.v1.LoggingService.InsertLogEntry
     */
    insertLogEntry: {
      name: "InsertLogEntry",
      I: InsertLogEntryRequest,
      O: InsertLogEntryResponse,
      kind: MethodKind.Unary,
    },
  }
};

