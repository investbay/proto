module go.investbay.dev/proto

go 1.21.7

require (
	connectrpc.com/connect v1.16.2
	google.golang.org/protobuf v1.34.1
)
